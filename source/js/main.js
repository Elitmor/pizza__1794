window.fn = {};

fn.blockKeys = function ($input) {
	$input.on('keydown', function (event) {
		var condition1 = !(event.which < 48 || event.which > 57)
		var condition2 = !(event.which < 96 || event.which > 105);
		var condition3 = event.which == 9 || event.which == 13 || event.which == 8;
		var condition = condition1 || condition2 || condition3;

		if (!condition) event.preventDefault();
	});
};

$(document).on('click', '.modal-close', function (event) {
	event.preventDefault();
	$.fancybox.close();
})

// $.fancybox.open('#product-modal', productModalOpt);

$('#top-slider').bxSlider({
	pager: false
});

$('#text-slider').bxSlider();

if ($('.check-item').length) {
	$('.check-item').masonry();
};

$('.product-ingredients.desc').width($('.product-capt').width());
$(window).load(function () {
	$('.product-ingredients.desc').width($('.product-capt').width());
});

/*====== hearder phone start ===== */
(function () {
	var
		$container = $('.header-phone'),
		$phone = $container.find('.phone'),
		$phoneDrop = $container.find('.phone-drop')
		;

	$phone.on('click', function () {
		$phoneDrop.toggle();
		return false;
	});

	$(document).on('click', function (event) {
		if (!$(event.target).closest($container).length) $phoneDrop.hide();
	});

})();
/*====== hearder phone start ===== */

/*====== basket start ===== */
function initCart() {
	var
		$basket = $('#basket-product'),
		$basketToggle = $basket.find('.basket-toggle')
		;

	function getItemSum() {

	};

	$basketToggle.on('click', function (event) {
		$basket.toggleClass('active')
		event.preventDefault();
	});
	changeQtyInit();
	deleteItemInit();
	deleteAllInit();
	hideCartInit();
};
initCart();
/*====== basket end ===== */


/*====== choose item start ===== */
(function () {

	var $container = $('.check-list-product');

	// if ( !$container.length && !$price.length ) return;

	var $item = $container.find('.item');
	var $listItem = $('.list-ingredients li');

	var $price = $('.product-price');
	var $priceSmall = $price.filter('.small');
	var $priceLarge = $price.filter('.large');

	var priceSmall = $priceSmall.find('strong').text() * 1;
	var priceLarge = $priceLarge.find('strong').text() * 1;
	var price = $price.find('strong').text() * 1;

	function getItemInfo(item) {
		return {
			price: item.find('.price').text().trim() * 1,
			name: item.find('.name').text().trim(),
			checked: item.hasClass('active')
		};
	};

	function getActiveItem() {
		return $item.filter('.active');
	};

	function getItemObjectInfo(list) {
		var arr = [];

		list.each(function (index, element) {
			arr.push(getItemInfo($(this)));
		});

		return arr;
	};

	function rounding(num) {
		if (num.toString().indexOf('.') < 0) return num + '.00';
		else return num
		return num;
	};

	function calculate() {
		setTimeout(function () {
			var additions = 0;
			var priceOne = 0;

			var count = $('.product-info .count-product .number').text() * 1;

			var $activeItem = getActiveItem();
			var itemObjInfo = getItemObjectInfo($activeItem);

			$.each(itemObjInfo, function () {
				additions += this.price;
			});

			var _price = price * count;
			var _priceSmall = (priceSmall + additions) * count;
			var _priceLarge = (priceLarge + additions) * count;

			$price.find('strong').text(rounding(_price));
			$priceSmall.find('strong').text(rounding(_priceSmall));
			$priceLarge.find('strong').text(rounding(_priceLarge));

		}, 10);
	};

	$container.on('click', '.item', function () {
		var $this = $(this);
		var index = $this.data('index');
		var $activeElem = $listItem.filter('.index' + index);

		$this.toggleClass('active');
		$this.hasClass('active') ? $activeElem.addClass('active') : $activeElem.removeClass('active');
		calculate();
	});

	; (function () {

		var $product = $('.product-info .count-product');
		var $number = $product.find('.number');

		$('.product-info .count-product .control').on('click', function () {
			var $this = $(this);
			var count = $number.text() * 1;

			$this.hasClass('increase') ? count++ : count--;

			if (count < 1) count = 1;

			$number.text(count);
			calculate();
		});

	})();

	$listItem.each(function (index) {
		var $this = $(this);
		var text = $this.text().replace(/ /gm, '');
		$this.data('index', index).addClass('index' + index);
		var $activeElem = $item.filter(function () { return text === $(this).text().replace(/ /gm, ''); });
		$activeElem.data('index', index).addClass('index' + index);

	}).on('click', function () {
		var $this = $(this);
		var index = $this.data('index');
		var $activeElem = $item.filter('.index' + index);
		$activeElem.trigger('click');
		$activeElem.hasClass('active') ? $this.addClass('active') : $this.removeClass('active');
	});


	$('.add_item').click(function () {
		var $product = $(this).closest('.product');

		var $size = $product.find('.check_size.active');
		var $checkedItem = $('.check-list-product').find('.item').filter('.active');

		var data = {
			item_id: $(this).data('id'),
			qty: $('.product-info .count-product .number').text() * 1
		};

		if ($checkedItem.length) {
			var custom_text = [];

			$checkedItem.each(function () {
				custom_text.push($(this).find('.name').text().trim());
			});

			data.price = $('.product-price').filter(':visible').find('strong').text();
			data.custom_text = custom_text.join('+');
		};

		if ($size.length) data.size = $size.hasClass('small') ? 'small' : 'large';

		if (window.location.href.indexOf('/pizza/') != -1) {
			data.custom = true;
			//data.price = parseFloat($('.product-price.active-price strong').html()/data.qty);
			data.price = parseFloat($('.product-price strong').html() / data.qty);
		};

		console.log(data)

		$.post('/cart/add_item/', data, function () {

			loadCart(function () {
				$('.basket-toggle').click();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			});
		});
		return false;
	});

})();

/*====== choose item end ======= */


/*====== form delivery start ======= */

(function () {

	function groupInput($container) {
		$container.each(function () {
			var $input = $(this).find('input[maxlength]');

			$input.each(function (index, element) {
				$(this).attr('data-index', index);
			});

			$input.on('input', function (event) {
				var $this = $(this);
				var maxlength = $this.attr('maxlength') * 1;
				var index = $this.attr('data-index') * 1;

				if (this.value.length >= maxlength)
					$input.filter('[data-index=' + (index + 1) + ']').focus();
			});

			$input.on('keydown', function (event) {
				var $this = $(this);
				var index = $this.attr('data-index') * 1;

				if (event.which === 8 && !this.value.length)
					$input.filter('[data-index=' + (index - 1) + ']').focus();
			});
		});
	};

	var $formDelivery = $('#form-delivery');

	if (!$formDelivery.length) return;

	var $groupInput = $('.group-input');
	var form = $formDelivery[0]

	var $phone = $([form.phone_code, form.phone_1])
		// $phoneCode 		= $(form.phone_code),
		// $phone1 		= $(form.phone_1),
		// $phone2 		= $(form.phone_2),
		// $phone3 		= $(form.phone_3)
		;

	groupInput($groupInput);
	fn.blockKeys($phone);

	function getData() {
		return {
			name: form.name.value,
			email: form.email.value,
			phone: form.phone_code.value + form.phone_1.value,
			address: form.address.value,
			message: form.message.value
		};
	};

	function validationName($this) {

		var illegaldReg = /\W/;
		var value = $this.val()

		var condition = value.length < 1 || value.length > 25;

		if (condition) {
			$this.closest('.form-group').addClass('invalid');
			return false;
		} else {
			$this.closest('.form-group').removeClass('invalid');
			return true;
		};
	};

	function validationEmail($this) {

		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		var value = $this.val()
		if (re.test(value) == false) {
			$this.closest('.form-group').addClass('invalid');
			return false;
		} else {
			$this.closest('.form-group').removeClass('invalid');
			return true;
		};
	};

	function validationPhone($this) {
		var data = getData();
		if (data.phone.length < 10) {
			$this.closest('.form-group').addClass('invalid');
			return false;
		} else {
			$this.closest('.form-group').removeClass('invalid');
			return true;
		};
	};

	$(form.name).on('blur', function () {
		validationName($(this));
	});

	$phone.on('blur', function () {
		validationPhone($(this));
	});

	$(form.address).on('blur', function () {
		validationName($(this));
	});

	// $(form.address2).on('blur', function () {
	// 	validationName( $(this) );
	// });

	// $(form.email).on('blur', function () {
	// 	validationEmail( $(this) );
	// });

	$formDelivery.on('submit', function (event) {
		var validName = validationName($(form.name));
		var validPhone = validationPhone($phone);
		//var validEmail = validationEmail(form.email);
		var validAddress = validationName($(form.address));
		//var validAddress2 = validationName($(form.address2));
		if (!(validName && validPhone && validAddress)) {
			event.preventDefault();
		} else {
			$.post('/cart/proceed_checkout/', getData(), function () {
				window.location.href = '/cart/checkout/thank-you/';
			});

		}
		return false;
	});

})();

/*====== form delivery end ========= */


/*====== google maps on page contacts start ======= */

(function () {
	if (typeof (google) != 'undefined') {

		var $mapWrap = $('.map-wrap');
		var $footer = $('#footer');

		function alignMap() {
			$mapWrap.height('');

			var offsetMapWrap = $mapWrap.offset().top;
			var offsetFooter = $footer.offset().top;

			$mapWrap.height(offsetFooter - offsetMapWrap);
		};

		$(window).on('load resize', alignMap);

		google.maps.event.addDomListener(window, 'load', function () {

			var markerImage = new google.maps.MarkerImage('/source/img/icons/target.png',
				new google.maps.Size(80, 80),
				new google.maps.Point(0, 0),
				new google.maps.Point(40, 40)
			);

			var map = new google.maps.Map(document.getElementById("google-map"), {
				center: new google.maps.LatLng(46.484093, 30.740250),
				zoom: 16,
				// scrollwheel: false,
				// disableDefaultUI: true
			});

			var adressMarker = new google.maps.Marker({
				position: new google.maps.LatLng(46.484093, 30.740250),
				map: map,
				icon: markerImage,
				title: "Кафе 1794",
				origin: 100
			});

			// google.maps.event.addListener(map, 'click', function (event) {
			//     console.log( event )
			//     console.log( map.getCenter() )
			// });

		});

	}
})();

function showPopup(message, submitCallback) {
	submitCallback = submitCallback || $.prompt.close();
	$.prompt(message, {
		top: '30%',
		buttons: { 'Спасибо': true },
		submit: submitCallback
	});
}

/*====== google maps on page contacts end ========= */

$('.reset a').click(function () {
	$('.contacts-form form')[0].reset();
	return false;
});
$('.send-comment').on('click', function (event) {
	var success = true;
	if ($('.contacts-form input[name="name"]').val() == '') {
		success = false;
		$('.contacts-form input[name="name"]').addClass('has-error');
	} else {
		$('.contacts-form input[name="name"]').addClass('has-success');
	}
	if ($('.contacts-form input[name="phone"]').val() == '') {
		success = false;
		$('.contacts-form input[name="phone"]').addClass('has-error');
	} else {
		$('.contacts-form input[name="phone"]').addClass('has-success');
	}
	if ($('.contacts-form textarea[name="message"]').val() == '') {
		success = false;
		$('.contacts-form textarea[name="message"]').addClass('has-error');
	} else {
		$('.contacts-form textarea[name="message"]').addClass('has-success');
	}
	if (success) {
		var data = {
			'name': $('.contacts-form input[name="name"]').val(),
			'phone': $('.contacts-form input[name="phone"]').val(),
			'message': $('.contacts-form textarea[name="message"]').val()
		}
		$.post('/send_contact/', data, function () {
			showPopup('Спасибо за обращение, мы свяжемся с вами');
		});
	}
	event.preventDefault();
});

$('.product-add-ingredients').on('click', function () {
	$('html, body').animate({ scrollTop: $('.check-item').offset().top })
});

function loadCart(callback) {
	$.post('/cart/get_data/', '', function (data) {
		$('#basket_holder').html(data);
		initCart();
		if (callback) {
			callback();
		}
	});
}

if ($('#basket_holder').length) {
	loadCart();
}

function changeQtyInit() {
	$('.plus, .minus').click(function (event) {
		var amount = $(this).parent().find('.amount').text();
		var item_id = $(this).parent().find('.amount').data('id');

		if ($(this).hasClass('plus')) {
			amount++;
		} else {
			amount--;
		}
		var data = {
			id: item_id,
			qty: amount
		}

		console.log(data)

		$.post('/cart/change_qty/', data, function () {
			loadCart(function () {
				$('.basket-toggle').click();
			});

		});
		event.preventDefault();
	});
}

function deleteItemInit() {
	$('.item .close').click(function () {
		$.post('/cart/remove_item/', 'id=' + $(this).data('id'), function () {
			loadCart(function () {
				$('.basket-toggle').click();
			});
		});
		return false;
	});
}

function deleteAllInit() {
	$('.delete_all').click(function () {
		$(this).parent().parent().remove();
		$.post('/cart/remove_all/', '', function () {
			loadCart();
		});
		return false;
	});
}

function hideCartInit() {
	$('.hide_cart').click(function () {
		$('.basket-toggle').click();
		return false;
	});
}

$('.product .check_size').click(function () {

	var $container = $(this).closest('.product-info');
	var $price = $('.product-price');

	$container.find('.size').removeClass('active');
	$(this).addClass('active');

	$price.removeClass('active-price').hide();

	if ($(this).hasClass('small')) {
		$price.filter('.small').addClass('active-price').show();
	} else {
		$price.filter('.large').addClass('active-price').show();
	};

});


$('.list-product .item .check_size').click(function () {

	var $this = $(this);
	var $container = $this.closest('.item');

	$container.find('.size').removeClass('active');
	$this.addClass('active');

	if ($this.hasClass('small_size')) {
		$container.find('.price_usual').show();
		$container.find('.price_large').hide();
	} else {
		$container.find('.price_usual').hide();
		$container.find('.price_large').show();
	};

});


; (function () {

	var $container = $('.product-info');
	if (!$container.length) return;

	var size = window.location.hash.replace(/#/, '').trim();
	var $selected = $container.find('.size').filter(function () {
		return $(this).text().trim().indexOf(size) >= 0 && size !== '';
	});

	// debugger;

	if ($selected.hasClass('small')) {
		$('.product-price.large').removeClass('active-price').hide();
		$('.product-price.small').addClass('active-price').show();
	} else {
		$('.product-price.large').addClass('active-price').show();
		$('.product-price.small').removeClass('active-price').hide();
	};

	$selected.length ? $selected.addClass('active') : $container.find('.size').eq(0).addClass('active');

})();



; (function () {

	var $cart = $('.cart-count');

	if (!$cart.length) return;

	var $input = $cart.find('input');
	var $control = $cart.find('.control');

	fn.blockKeys($input);

	$input.on('blur', function () {
		if (this.value < 1) this.value = 1;
	});

	$control.on('click', function (event) {
		event.preventDefault();
		var $inp = $(this).closest('.cart-count').find('input');

		if ($(this).hasClass('add')) {
			$inp.val($inp.val() * 1 + 1);
		} else {
			$inp.val($inp.val() * 1 - 1);
		};

		if ($inp.val() * 1 > 999) $inp.val(999);
		if ($inp.val() * 1 < 1) $inp.val(1);
	});

})();


$('.list-product .item .add_to_cart').click(function (event) {
	var $this = $(this);
	var $container = $this.closest('.item');
	var url = $this.data('url').trim();

	if (url.indexOf('pizza') != -1) {
		event.preventDefault();
		var size = parseFloat($container.find('.size.active').text().trim());
		var url = $(this).data('url');
		window.location = url + '#' + size;
	};
});


$('.large_cart .add, .large_cart .remove').click(function () {
	var amount = $(this).parent().find('input').val();
	var item_id = $(this).parent().find('input').data('id');
	var price = $(this).parent().find('input').data('price');
	var data = {
		id: item_id,
		qty: amount
	}
	var me = $(this);
	$.post('/cart/change_qty/', data, function (response) {
		$('.total_price').html(response.total);
		me.parent().next().find('.total_row').html(amount * price);
	});
	return false;
});

$('.large_cart .cart-remove').click(function () {
	$(this).parent().remove();
	$.post('/cart/remove_item/', 'id=' + $(this).data('id'), function (response) {
		$('.total_price').html(response.total);
		if (response.total == 0) {
			showPopup('Вы удалили все товары');
			window.setTimeout(function () {
				window.location = '/';
			}, 2000);
		}
	});
	return false;
});

$('.delete_all_items').click(function () {
	$.post('/cart/remove_all/', '', function () {
		window.location = '/menu/pizza/';
	});
	return false;
});

$('.to-checkout').click(function () {
	window.location = '/cart/checkout/';
	return false;
});

$('.cart-image a').on('click', function (event) {
	var data = $(this).closest('.cart-image').data();

	$.fancybox.open('#product-modal', {
		minWidth: 850,
		closeBtn: false
	});

	$('#product-modal .img').attr('src', data.img);
	$('#product-modal .name').text(data.name);
	$('#product-modal .reference').text(data.reference);
	$('#product-modal .ingredients-main').text(data.ingredients_main);
	$('#product-modal .ingredients').text(data.ingredients);

	event.preventDefault();
});

$('.cart-name').each(function () {
	var $this = $(this);
	$this.text($this.text().replace('()', ''));
});

//$(document).snowfall({image :"/source/img/flake.png", minSize: 10, maxSize:52, minSpeed: 1, maxSpeed: 6, flakeCount : 35});



(function () {
	var $groups = $('.order-form .form-group');
  
	makeMobileView();
	$(window).on('resize', makeMobileView);
	
	function makeMobileView () {
	  $groups.each(function () {
		var $description = $(this).find('.description');
		var $input = $(this).find('.input:not([maxlength="3"])');
		
		if ($(window).width() < 768) {
		  $description.hide();
		  $input.attr('placeholder', $description.text());
		} else {
		  $description.show();
		  $input.attr('placeholder', '');
		}
	  });
	}
  })();